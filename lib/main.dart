import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:task1/models/auth_request.dart';
import 'package:task1/models/auth_response.dart';
import 'package:dio/dio.dart';
import 'package:task1/services/api_service.dart';

void main() {
  const myApp = MyApp();
  runApp(myApp);
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      title: 'Auth App',
      home: AuthScreen(),
    );
  }
}

class AuthScreen extends StatefulWidget {
  const AuthScreen({super.key});

  @override
  _AuthScreenState createState() => _AuthScreenState();
}

class _AuthScreenState extends State<AuthScreen> {
  final AuthService _authService = AuthService(Dio());
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  String _token = '';
  String _tokenExpiry = '';
  String _refreshToken = '';
  int _cabinetUserId = 0;
  String _refreshTokenEndTime = '';
  bool _cabinetUserIsAdmin = false;

  Future<void> _login() async {
    final String email = _emailController.text;
    final String password = _passwordController.text;
    const String appId = 'webchat';
    final AuthRequest authData =
        AuthRequest(password: password, email: email, appId: appId);

    try {
      final ResponseData<AuthResponseData> authResponse =
          await _authService.login(authData);
      if (authResponse.success) {
        final data = authResponse.data;
        setState(() {
          _token = data.accessToken;
          _tokenExpiry = DateFormat('yyyy-MM-dd HH:mm:ss').format(
              DateTime.fromMillisecondsSinceEpoch(
                  data.accessTokenEndTime * 1000));
          _refreshToken = data.refreshToken;
          _cabinetUserId = data.cabinetUserId;
          _refreshTokenEndTime = DateFormat('yyyy-MM-dd HH:mm:ss').format(
              DateTime.fromMillisecondsSinceEpoch(
                  data.accessTokenEndTime * 1000));
          _cabinetUserIsAdmin = data.cabinetUserIsAdmin;
        });

        _showSnackBar(
            'Token: $_token\nToken Expiry: $_tokenExpiry\nrefreshTokenEndTime: $_refreshTokenEndTime\nrefreshToken: $_refreshToken\ncabinetUserIsAdmin: $_cabinetUserIsAdmin\ncabinetUserId: $_cabinetUserId');
      } else {
        _showSnackBar('wrong login or password');
      }
    } on DioError catch (e) {
      _showSnackBar('wrong login or password');
    }
  }

  void _showSnackBar(String message) {
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: Text(message),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Auth Screen'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            TextField(
              controller: _emailController,
              keyboardType: TextInputType.emailAddress,
              textInputAction: TextInputAction.next,
              decoration: const InputDecoration(
                labelText: 'Email',
              ),
            ),
            TextField(
              controller: _passwordController,
              obscureText: true,
              decoration: const InputDecoration(
                labelText: 'Password',
              ),
              onEditingComplete: () => _login(),
            ),
            const SizedBox(
              height: 15,
            ),
            ElevatedButton(
              onPressed: _login,
              child: const Text('Login'),
            ),
          ],
        ),
      ),
    );
  }
}
