// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'auth_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ResponseData<T> _$ResponseDataFromJson<T>(
  Map<String, dynamic> json,
  T Function(Object? json) fromJsonT,
) =>
    ResponseData<T>(
      json['success'] as bool,
      fromJsonT(json['data']),
    );

Map<String, dynamic> _$ResponseDataToJson<T>(
  ResponseData<T> instance,
  Object? Function(T value) toJsonT,
) =>
    <String, dynamic>{
      'success': instance.success,
      'data': toJsonT(instance.data),
    };

_$_AuthResponseData _$$_AuthResponseDataFromJson(Map<String, dynamic> json) =>
    _$_AuthResponseData(
      cabinetUserId: json['cabinetUserId'] as int,
      accessToken: json['accessToken'] as String,
      accessTokenEndTime: json['accessTokenEndTime'] as int,
      refreshToken: json['refreshToken'] as String,
      refreshTokenEndTime: json['refreshTokenEndTime'] as int,
      cabinetUserIsAdmin: json['cabinetUserIsAdmin'] as bool,
    );

Map<String, dynamic> _$$_AuthResponseDataToJson(_$_AuthResponseData instance) =>
    <String, dynamic>{
      'cabinetUserId': instance.cabinetUserId,
      'accessToken': instance.accessToken,
      'accessTokenEndTime': instance.accessTokenEndTime,
      'refreshToken': instance.refreshToken,
      'refreshTokenEndTime': instance.refreshTokenEndTime,
      'cabinetUserIsAdmin': instance.cabinetUserIsAdmin,
    };
