import 'package:retrofit/retrofit.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:dio/dio.dart' hide Headers;

import '../models/auth_request.dart';
import '../models/auth_response.dart';
part 'api_service.g.dart';

@RestApi(baseUrl: 'https://api.chatapp.online/v1')
abstract class AuthService {
  factory AuthService(Dio dio) => _AuthService(dio);

  @POST('/tokens')
  @Headers({'Content-Type': 'application/json'})
  Future<ResponseData<AuthResponseData>> login(
    @Body() AuthRequest authRequest,
  );
}
