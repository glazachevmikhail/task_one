// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'auth_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_AuthRequest _$$_AuthRequestFromJson(Map<String, dynamic> json) =>
    _$_AuthRequest(
      password: json['password'] as String,
      email: json['email'] as String,
      appId: json['appId'] as String,
    );

Map<String, dynamic> _$$_AuthRequestToJson(_$_AuthRequest instance) =>
    <String, dynamic>{
      'password': instance.password,
      'email': instance.email,
      'appId': instance.appId,
    };
